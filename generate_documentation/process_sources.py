import os
import sys
import subprocess

args = "python3 -m jupyter nbconvert --to python filename".split()

def batch_rename(dirname):
    for filename in os.listdir(dirname):
         filename = os.path.join(dirname, filename)

         if os.path.isdir(filename):
             batch_rename(filename)
         elif filename.endswith("ipynb.txt"):
             new_filename = filename[0:-4]
             os.rename(filename, new_filename)

             args[6] = new_filename
             subprocess.run(args)

batch_rename("./_build")

