$(function() {

$(".wy-breadcrumbs-aside a").each(function() {
  str = $(this).attr("href");
  str_ipynb = str.replace(/ipynb\.txt$/g, 'ipynb');
  str_py = str.replace(/ipynb\.txt$/g, 'py');
  if (str != str_ipynb) {
    $(this).attr("href", str_ipynb);
    $('footer').prepend('<div id="download_files">Download files</div>');
    $('#download_files').html(
    '<a href="' + str_py +
    '">Download this page as a Python script file</a>'
    + " | " +
    '<a href="' + str_ipynb +
    '">Download this page as a Jupyter Notebook</a>'
    );
  }
});

});
