.. python-intro-tutorial documentation master file, created by
   sphinx-quickstart on Thu Nov  9 14:37:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Simulation study on Python with sstudy package
==========================================================

One important aspect of proposing new machine learning/Statistical estimators and methods is the performance test phrase. With that in mind, we present here a short introduction on package `sstudy` that facilitates such procedure.

Citation
********

If you use this software, please cite it as: ::

    @misc{2004.14479,
    Author = {Marco H A Inácio},
    Title = {Simulation studies on Python using sstudy package with SQL databases as storage},
    Year = {2020},
    Eprint = {arXiv:2004.14479},
    }


Topics
=================================================

.. toctree::
    :maxdepth: 1
    :numbered:

    sections/sstudy


Full package documentation
=================================================
.. toctree::
    :maxdepth: 4

    documentation
