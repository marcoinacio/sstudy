FROM python:3.7

# Install dependencies
RUN apt-get update
RUN apt-get install -y --no-install-recommends libsqlite3-dev git
RUN pip install torch==1.5.1+cpu torchvision==0.6.1+cpu -f https://download.pytorch.org/whl/torch_stable.html
RUN pip install sstudy pandas ipython scipy npcompare sklearn matplotlib nnlocallinear pystan
RUN git clone https://gitlab.com/marcoinacio/sstudy.git

#-------------------------------------

# Run basic example
WORKDIR sstudy/examples/basic
RUN python run_simulation_study.py& python run_simulation_study.py& wait
RUN python explore_results.py

#-------------------------------------

# Run hypothesis testing example
WORKDIR ../hypothesis_testing
# optional line to reduce number of simulations
RUN sed -i 's/no_simulations = 1000/no_simulations = 15/g' run_simulation_study.py
RUN python run_simulation_study.py& python run_simulation_study.py& wait
RUN python table_results.py
RUN python plot_results.py

#-------------------------------------

# Run neural networks example
WORKDIR ../neural_networks
# optional line to reduce number of simulations
RUN sed -i 's/seed = range(1200)/seed = range(5)/g' run_simulation_study.py
# optional line to run only a single epoch (fast and very low quality neural net)
RUN sed -i 's/es_max_epochs = 100/es_max_epochs = 1/g' run_simulation_study.py
RUN python run_simulation_study.py
RUN python explore_results.py

#-------------------------------------

# Run density estimation example
WORKDIR ../density_estimation
# optional line to reduce number of simulations
RUN sed -i 's/no_simulations = 30/no_simulations = 4/g' run_simulation_study.py
# optional line to run a fast (and very low quality) MCMC
RUN sed -i 's/sampleposterior(10_000/sampleposterior(50, tolrhat=1.0/g' run_simulation_study.py
RUN python run_simulation_study.py
RUN python explore_results.py

#-------------------------------------
WORKDIR ..
CMD ["bash"]

# you can now get the result pdf plots on each example folder
# e.g.:
# docker build . -t docker_image_name
# docker run --rm -it docker_image_name
# # (then inside the container ls the desired folder)
# ls hypothesis_testing/*.pdf
# # (will output)
# # (hypothesis_testing/null.pdf  hypothesis_testing/power.pdf)

